FROM ubuntu:16.04

ARG TERRAFORM_VERSION=0.11.10
ARG GO_VERSION=1.11.2
ARG PACKER_VERSION=1.3.2

WORKDIR /tmp/terratesting
COPY . .

RUN apt-get update
RUN apt-get -qq install -y \
    build-essential \
    unzip \
    wget

# install terraform
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
	&& unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin \
	&& terraform --version

# install go
RUN wget https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz \
	&& tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz \
	&& echo 'export PATH=$$PATH:/usr/local/go/bin' | tee -a /etc/profile

# install packer
RUN wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip \
	&& unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /usr/local/bin/ \
	&& packer --version

CMD ["/bin/bash"]